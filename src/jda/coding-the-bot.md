# Coding the bot

Okay, now, to finally coding the bot!
We'll create the main class.

First things first, in the file viewer, expand `src` and expand `main` within it, and right-click on java then hover over `New`, and click on `Java Class`.

![Creating Java Class](./imgs/creating_java_class.jpg)


Call your main class any name and click `Ok` like this:

![Naming Java Class](./imgs/naming_java_class.jpg)

Now create a main method in the class you just created, `Hint: type psvm<tab>`.

![Creating main method](./imgs/creating_main_method.jpg)

Now we're going to create our JDA instance in the main method!

In the main method, type:

```java
JDA jda = new JDABuilder("token").build();
        // it'd be better if you actually kept it in a 
        // .txt or .json config file and grabbed
        // the token from there.
        // blocks the bot until jda is fully ready
    jda.awaitReady();
```

This will create a JDA instance for your bot, also, if the lines are red, make sure to import everything, but! In case anything goes wrong with the token or anything else, we're going to go to our main method.

We are going to add `throws LoginException` right after our main method and before the curly brace.

The full code would look like this:

```java
public class Main {
  public static void main(String[] args) throws LoginException, InterruptedException {
    JDA jda = new JDABuilder("token").build();

    jda.awaitReady();
  }
}
```

The `LoginException` is in case something wrong goes off, it'll throw that error :)

And the `InterruptedException` is in case the bot gets interrupted when its blocking until its ready.

> **IMPORTANT:** BE SURE NOT TO GIVE THE TOKEN TO ANYONE. EVER.

So that's why I'm going to teach you how to make your token a little bit more secure!

First, make a new file anywhere you want! call it `token.txt` and don't put anything in it other than the token!
and then, we would need to read the token from the file! we'll use the packages `java.util.Scanner` and `java.io.File` to do it!

We would need to try to read the file in a try/catch block, and assign the file to a variable, like this:

```java
  File tokenFile = new File("C:\users\user\documents\token.txt");
 String token = null;
  
try(Scanner s = new Scanner(tokenFile)) {
 if(s.hasLine()) {
 token = s.hasNextLine(); 
 } else {
 throw new RuntimeException("The token file is empty");
}
 catch (IOException e) {

System.out.println("File not found");
  }
}
```

Use this code just in the main method before the JDA instance!

> **NOTICE:** In here we supposedly created a file and put it anywhere we like, so you'd need to use your `token.txt`'s path wherever you put it!

```java
public static void main(String[] args)
throws LoginException {
  JDA jda = new JDABuilder(token).build();
  jda.awaitReady();
 }
```

There we are!


Now, click the green play button to the left of our class name and select `Run`.

You'll see something like this:

![Expected Results](./imgs/first_run_expected_results.jpg) 

The first 6 lines are because we dont have any `slf4j` implementations, *yet*.

The next 3 lines tell us that JDA has successfully logged in to discord, and is ready to receive messages. But our bot doesn’t do anything right now.

Now look in the guild that your bot is in! If its online, HOORAY! Everything's working! If it's not, follow the tutorial and check the code out again or ask in [**`jvm-help`**](https://discordapp.com/channels/265828729970753537/651854967778443265) in our server's channel!

---

Let's move on to [**Commands, Listeners and Activities**](./commands-listeners-and-activities.html).
