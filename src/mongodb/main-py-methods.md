# Main Python methods

The main methods you will probably use the most are `collection.insert_one`, `collection.find_one`, `collection.update_one` (there are also "multi" variants of those methods which will use cursors and batch operation models). `collection.aggregate` is also really useful and uses the [Aggregation](https://docs.mongodb.com/manual/aggregation/) feature present in MongoDB.

Here are the covered topics:

- [Inserting](#inserting)
- [Querying](#querying)
- [Updating](#updating)

## Inserting

First thing you need is to insert documents. This is super simple - all you need is a collection. Here's a little example of what inserting an document for guild configuration could look like:
```py
# "coll" is our collection

await coll.insert_one(
    {
    "_id": 1234  # guild ID
    }
)
```

Now you want to insert multiple documents for some feature that needs to do so. You shouldn't go for a `for` loop with singular inserts as that would needlessly overload the MongoDB instance with lots of operations. What you can do instead is use a "multi" operation and provide how many documents you may want to insert. The code for it is quite simple as well. Here's a little example:
```py
await coll.insert_many(
    (
        {
            "_id": 1234
        },
        {
            "_id": 5678
        },
        {
            "_id": 9012
        },
    )}
)
```

## Querying

Let's make a simple query for some documents. Imagine we have a collection that represents guild configurations and its schema is like this
```json
{
    // The values represented here are completly arbitrary
    // and just represent the value type
    "_id": 1234,  // guild ID
    "prefix": "foo!"
    "some_feature": false,
    "some_other_feature": {
        "enabled": true,
        "some_option": true,
        "some_other_option": 42
    }
}
```

Let's say you wanted to get whole guild object to check some values in it. Here's what you'd have to do:
```py
# "coll" if our guild config collection
# and "guild_id" is, well, an ID of a guild

guild_conf = await coll.find_one({"_id": guild_id})
```
Note that you use actual dicts to make the query and not just a string representation of them.

And what if you just want to get the `some_other_feature` embedded document? It's also very simple:
```py
some_other_feature_conf = await coll.find_one(
    {"_id": guild_id},  # query
    {"some_other_feature": 1}  # field projection
    )
```
And that would return a dict like this
```json
{
    "_id": 1234,  // guild ID
    "some_other_feature": {
        "enabled": true,
        "some_option": true,
        "some_other_option": 42
    }
}
```
instead of the whole document.

## Updating

Now, imagine you want to update a document. You want to change the prefix and some embedded field on the `some_other_feature` embedded document. Here's how you'd do it:
```py
# "coll" is our guild config collection
# and "guild_id" is some guild's ID

results = await coll.update_one(
    {"_id": guild_id},  # query to select document to update
    {  # update operations
        "prefix": "bar!",
        "some_other_feature.enabled": False
    }
)
```
This would also return an `UpdateResult` object which contains some useful info on the update operation.