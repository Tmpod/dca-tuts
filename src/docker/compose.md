# Docker Compose

Ok, time for a new file `docker-compose.yml`
```yml
version: "3"
services:
  bot:
    build: .
    environment:
      - TOKEN
```

Previously we were invoking `docker build` and `docker run` directly, which is fine when your doing simple stuff but you may of noticed once we added the token to the run command it got quite long and it could get even longer in the future once you learn more features of Docker. But Docker has another kind of file that lets you put all this information that would normally be part of the `docker build` and `docker run` commands into a single file.

# Installing Docker Compose

Docker compose is actually a Python application and normally needs installing separately from Docker, possibly the easiest and safest way to install it on a Linux computer is searching for and installing it from your package manager.

![dnf compose](./imgs/11.png)

But you might not be so lucky though but you can also install it using pip like any other Python module.
```
pip3 install docker-compose
```
Whether you are installing from your package manager or from pip you should now have access to the `docker-compose` command.

So you got Docker Compose installed, the `docker-compose.yml` file made and put with the rest of your files? Good

Now time for some magic, but first you will need the `TOKEN` environment variable, you can use either the `export` command on Linux or the `set` command on Windows.
```
export TOKEN=your_bots_token_here
# may want to add that export command to your ~/.bashrc
```
Now run `docker-compose config` to see how `docker-compose` interprets the `docker-compose.yml` file. 

![compose config](./imgs/12.png)

You should get output like that. Now for explaining the docker-compose.yml
- `version:` is the Docker API version that the compose file is written to be compatible with, it generally doesn't matter too much. But if you care, heres a link [about versions](https://docs.docker.com/compose/compose-file/compose-versioning/)
- `services:` yes that's 'services' with an 's' at the end which means you can set up multiple Docker containers with a single Docker Compose file
- `bot:` this is just a name for that individual service, to identify it in logs or other config that may come later. You can safely rename this to what ever you want.
- `build:` this is telling Docker that I actually want to have an image built then after the build I have `.` which is the build context. Works the same as it does for `run`, it is where Docker will look for the Dockerfile and what ADD will have access to.
- `environment:` this is where you will list the environment variables that you want inside the container, here I have `TOKEN` just typing a variable name by its self, Docker Compose will search the variables in your shell to get the answers it needs. You can also use the `VARIABLE=value` notation to specify an environment variable that you want to be stored in the `docker-compose.yml` file. Such at `DOCKER=true`, maybe your bot does something special when it sees that variable?
```
docker-compose up
```
This is where the real magic happens, `docker-compose` will read your file then build what ever images need building etc and then hopefully begin running your bot.

![compose up](./imgs/13.png)

Much excite, very wow and now you should be able to interact with your bot through discord! Note: if you exit and do `docker-compose up` again it will not rebuild the service if it already exists, if you want to force it to rebuild you can use
```
docker-compose up --build
```
![compose logs](./imgs/14.png)

`docker-compose logs` will show logs from all services created by that Docker Compose file. Each log is prefixed with a name, see it's the same name as the service in the `docker-compose.yml`
```
docker-compose down
```
will remove all the stuff that Docker Compose created.